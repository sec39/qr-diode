from pyzbar import pyzbar
import argparse
import datetime
import imutils
import time
import pathlib
import json
import base64
import hashlib
from imutils.video import VideoStream
import cv2

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-o", "--outputdir", type=str, default="~/Downloads",
	help="path of output folder where received files are written to, default: ~/Downloads")
ap.add_argument("-d", "--devicenumber", type=int, default=0,
	help="video device number, /dev/videoX, default: /dev/video0")
args = vars(ap.parse_args())

# initialize the video stream and allow the camera sensor to warm up
print("[INFO] starting video stream...")
# vs = VideoStream(src=0).start()
vs = VideoStream(src=args["devicenumber"]).start()
time.sleep(2.0)

# open the log CSV file for writing
log = open(pathlib.Path.home().joinpath('.qr_receive.log'), "w")

# set for found barcodes (successfully interpreted qr codes actually)
found = set()
datablocks = dict()

# variables for a file to receive
filename = None
shasum = None
blocks = None
fileready = False

# loop over the frames from the video stream
while True:
    # grab the frame from the threaded video stream and resize it to
    # have a maximum width of 1000 pixels
    frame = vs.read()
    frame = imutils.resize(frame, width=1000)
    
    # find the barcodes in the frame and decode each of the barcodes
    barcodes = pyzbar.decode(frame)
    
    # loop over the detected barcodes
    for barcode in barcodes:
        # extract the bounding box location of the barcode and draw
        # the bounding box surrounding the barcode on the image
        (x, y, w, h) = barcode.rect
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 255), 2)
        
        # the barcode data is a bytes object so if we want to draw it
        # on our output image we need to convert it to a string first
        barcodeData = barcode.data.decode("utf-8")
        barcodeType = barcode.type
        
        # draw the barcode data and barcode type on the image
        text = "{} ({})".format(barcodeData, barcodeType)
        cv2.putText(frame, text, (x, y - 10),
            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
        
        # try to decode json from barcode
        try:
            msg = json.loads(barcodeData)
            #print(msg)
            
            if ("file" in msg): # this is start message
                # cleanup old data, setup receive file params
                found = set()
                datablocks = dict()
                filename = msg["file"]
                filename = (pathlib.Path(filename)).name
                shasum = msg["sha256"]
                blocks = int(msg["blocks"])
                fileready = False
                print("[INFO] new file to receive: {}, blocks={}".format(filename, blocks))
            
            # if the barcode text is currently not in dict, write
            # the timestamp + barcode to log and update the set
            if barcodeData not in found:
                log.write("{},{}\n".format(datetime.datetime.now(),
                    barcodeData))
                log.flush()
                found.add(barcodeData)
            
            if ("s" in msg): # this is data message
                # store data block, format <sequence_no> = b<decoded base64 data>
                datablocks[int(msg["s"])] = base64.b64decode(msg["d"])
                print("[INFO] Add block #{}".format(msg["s"]))
                
                if (blocks == int(msg["s"]) and not fileready): # last block!!
                    # append datablocks and write to destination file
                    print("[INFO] All blocks received, file: {}".format(filename))
                    # mark job ready
                    fileready = True
                    # write file if checksum is ok
                    # set path
                    path = str(pathlib.Path(args["outputdir"]).joinpath(filename))
                    if (args["outputdir"] == '~/Downloads'):
                        path = str(pathlib.Path.home().joinpath('Downloads', filename))

                    # checksum calculation
                    allfile = bytearray()
                    for i in range(1, blocks + 1):
                        if (i in datablocks):
                            allfile = allfile + datablocks[i]
                    newsha = hashlib.sha256(bytes(allfile)).hexdigest();
                    if (shasum == newsha):
                        print("[INFO] sha256 match, writing new file to disk: {}".format(filename))
                        log.write("filename: {}, sha256: {}".format(filename, newsha))
                        with open(pathlib.Path(path).resolve(), "wb") as f:
                            f.write(bytes(allfile))
                    else:
                        print("[ERROR] sha256 checksum failure, abort writing to disk: {}".format(filename))
            
        except Exception as e: 
            print(e)
                
    # show the output frame
    cv2.imshow("QR-code Scanner", frame)
    key = cv2.waitKey(1) & 0xFF
    
    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break

# close the output CSV file do a bit of cleanup
print("[INFO] cleaning up...")
log.close()
cv2.destroyAllWindows()
vs.stop()
