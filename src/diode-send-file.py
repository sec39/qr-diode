import qrcode
import numpy as np
from random import random
import base64
import argparse
import hashlib
import cv2

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-f", "--file", type=str, required=True,
	help="path to file to send")
args = vars(ap.parse_args())

# list of binary chunks to send
chunks = []
allfile = bytearray()
shasum = None

# read file
def read_in_chunks(file, chunk_size=384):  # Default chunk size: 512.
    while True:
        chunk = file.read(chunk_size)
        if chunk:
            yield chunk
        else: # The chunk was empty, which means we're at the end of the file
            return

with open(args["file"], "rb") as f:
    for chunk in read_in_chunks(f):
        #print(chunk)
        chunks.append(chunk)
        allfile = allfile + bytearray(chunk)
    # calculate sha256
    shasum = hashlib.sha256(bytes(allfile)).hexdigest();
    print("[INFO] file '{}' sha256 is {}".format(args["file"], shasum))

def get_encoded_string(bytes):
    base64_bytes = base64.b64encode(bytes)
    base64_string = base64_bytes.decode("utf-8")
    return base64_string


# First data to be encoded - file description
data = '{{"file":"{}","sha256": "{}","blocks":{}}}'.format(args["file"], shasum, len(chunks))
print("[INFO] fileinfo message: '{}'".format(data))

# Encode data
qr = qrcode.QRCode(version = None,
                   box_size = 10,
                   border = 5)
qr.add_data(data)
qr.make(fit=True)
img = qr.make_image(fill_color = 'black',
                    back_color = 'white').convert('RGB')
cvImg = np.asarray(img)
#cvImg = cvImg[:, :, ::-1].copy()

cv2.startWindowThread()
cv2.namedWindow("qrcode")
cv2.imshow('qrcode', cvImg)
cv2.waitKey(4000)

seq = 0
for c in chunks:
    seq = seq +1
    data = '{{"s":"{}","d":"{}"}}'.format(seq, get_encoded_string(c))
    #print(data)
    
    # create updated qr
    qr.clear()
    qr.add_data(data)
    qr.make(fit=True)
    img = qr.make_image(fill_color = 'black',
                    back_color = 'white').convert('RGB')
    cvImg = np.asarray(img)
    
    # show data
    cv2.imshow('qrcode', cvImg)
    cv2.waitKey(330)
