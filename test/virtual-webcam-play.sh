#!/bin/bash
#
# Play video recording to virtual webcam device
#
ffmpeg -stream_loop -1 -re -i output.mkv -vcodec rawvideo -threads 0 -f v4l2 -vf format=yuv420p /dev/video2
