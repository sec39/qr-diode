
# Virtual webcam testing

## Kernel modules

sudo modprobe v4l2loopback card_label="My Fake Webcam" exclusive_caps=1

## Link video stream to fake webcam

```
$ ls -1 /dev/video*
/dev/video0
/dev/video1
/dev/video2
```

Test with: `ffplay /dev/videoX` --> should spawn a window with webcam on

Next up, we need to “fake” our webcam feed with :

```
$ ffmpeg -stream_loop -1 -re -i /path/to/video -vcodec rawvideo -threads 0 -f v4l2 /dev/videoX
```

## Cleanup

```
$ sudo modprobe --remove v4l2loopback
```
