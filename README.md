# Simple data diode with QR-codes for Linux

Linux releases (with amd64 binaries): https://gitlab.com/sec39/qr-diode/-/releases

## Concept

To transfer a tiny amount of data to/from air-gapped computer.

**Why?** Maybe offline wallets or offline CA keys.

Practical data transfer limit is few dozens of kilobytes.

Example session of `diode-send-file`:

![](qr-diode-send-file.gif)

*Datafile is shown as a series of qr-codes. First code is file metadata (checksum).*

```
SOURCE-WORKSTATION    <--image_capture--    :USB-WEBCAM--------DESTINATION-WORKSTATION
(1)                                                            (2)
```

Example session of `diode-receive`:

![](qr-diode-receive-file.gif)

*Preview window (of usb webcam) shows all recognized qr-codes. All complete and checksummed files a written to disk*.

Summary:

* Source workstation:
  * Target file --> base64 --> split to chunks --> qr-encode chunks
  * Show qr-codes on-screen
* Destination workstation:
  * Read videostream
  * Take a frame
  * Recognize and read qr-codes
  * Combine data and write to a file
  
## Usage

### Prepare to transfer data

* Source workstation
  * Working USB webcam
  * Webcam pointed to screen of source workstation
  * Execute: `diode-receive`

### diode-receive

Interprets QR-codes and writes all valid files to target directory.

```
./diode-receive -o <target_dir> [-d <video_device_number>]
```

### diode-send-file

Sends a file to destination workstation (by creating a number of qr codes).

```
./diode-send-file -f <path_to_a_file>
```

### Creating binaries

* `python -m venv venv`
* `source venv/bin/activate`
* `pip install --upgrade pip`
* `pip install opencv-python==4.5.5.64` see below
* `pip install imutils Pillow qrcode pyzbar`
* `pip install PyQt5`
* `pip install --upgrade pyinstaller`
* `dist-pack.sh`

Binaries are created at directory `./dist`. Copy directory contents (including libraries) to `$HOME/bin` or `/usr/local/bin`.

## Dependencies / Notes

* `pip install opencv-python` note! decoding in CPU. CUDA-enabled version, see pkg docs! https://pypi.org/project/opencv-python/
   * for now, maybe `pip install opencv-python==4.5.5.64` to fix importerror? https://github.com/opencv/opencv-python/issues/680
* `pip install imutils Pillow qrcode`
* `pip install PyQt5`

