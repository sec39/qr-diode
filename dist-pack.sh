#!/bin/bash
#
set -eu

rm -rf dist build
rm *spec

pyinstaller \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libQt5XcbQpa-ca221f44.so.5.15.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/cv2/qt/plugins/platforms/libqxcb.so:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libfreetype-c0e61f0c.so.6.14.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libX11-xcb-69166bdf.so.1.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-icccm-413c9f41.so.4.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-image-e82a276d.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-shm-7a199f70.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-keysyms-21015570.so.1.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-randr-a96a5a87.so.0.1.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-render-util-43ce00f5.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-render-637b984a.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-shape-25c2b258.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-sync-89374f40.so.1.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-xfixes-9be3ba6f.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-xinerama-ae147f87.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-xkb-9ba31ab3.so.1.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxkbcommon-x11-c65ed502.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxkbcommon-71ae2972.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-util-4d666913.so.1.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libXau-00ec42fe.so.6.0.0:.' \
--distpath dist/bin-diode-receive src/diode-receive.py 

pyinstaller \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libQt5XcbQpa-ca221f44.so.5.15.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/cv2/qt/plugins/platforms/libqxcb.so:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libfreetype-c0e61f0c.so.6.14.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libX11-xcb-69166bdf.so.1.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-icccm-413c9f41.so.4.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-image-e82a276d.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-shm-7a199f70.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-keysyms-21015570.so.1.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-randr-a96a5a87.so.0.1.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-render-util-43ce00f5.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-render-637b984a.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-shape-25c2b258.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-sync-89374f40.so.1.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-xfixes-9be3ba6f.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-xinerama-ae147f87.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-xkb-9ba31ab3.so.1.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxkbcommon-x11-c65ed502.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxkbcommon-71ae2972.so.0.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libxcb-util-4d666913.so.1.0.0:.' \
--add-binary 'venv/lib/python3.6/site-packages/opencv_python.libs/libXau-00ec42fe.so.6.0.0:.' \
--distpath dist/bin-diode-send-file src/diode-send-file.py

cd dist
ln -sf bin-diode-receive/diode-receive/diode-receive diode-receive
ln -sf bin-diode-send-file/diode-send-file/diode-send-file diode-send-file

# pack
tar jcvf ../qr-diode-v2.tar.bz2 *
sha256sum ../qr-diode-v2.tar.bz2 > ../qr-diode-v2.tar.bz2.sha256 
cd ..
